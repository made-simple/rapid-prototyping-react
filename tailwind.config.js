const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = { theme: defaultTheme, variants: ['responsive', 'hover'], plugins: [] };
