function toRad(num: number) {
    return num * Math.PI / 180;
}
export function calculateDistance(coor1: Coordinates, coor2: Coordinates) {
    const { latitude: lat1, longitude: lon1 } = coor1;
    const { latitude: lat2, longitude: lon2 } = coor2;
    var R = 6371; // km
    var dLat = toRad(lat2 - lat1);
    var dLon = toRad(lon2 - lon1);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
}