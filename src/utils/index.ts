import dayjs, { Dayjs } from 'dayjs'
export const CURRENT_LOCATION_KEY = 'current';

export const isInViewport = function (elem: HTMLElement) {
    var bounding = elem.getBoundingClientRect();
    return (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};



export function isToday(date: Dayjs) {
    const comparisonKey = ['$y', '$D', '$M']
    const now = dayjs()

    return comparisonKey.filter(function (key) {
        return (now as any)[key] !== (date as any)[key]
    }, date).length === 0
}


export function scrollTo(wrapper: HTMLElement, id: string) {
    // const bounding = wrapper.querySelector(`[id="${id}"]`).getBoundingClientRect();
    // //Safari gives 0 for the first location and chrome -400
    // wrapper.scrollLeft += bounding.left;
    wrapper.querySelector(`[id="${id}"]`).scrollIntoView();
}


export function getCurrentHash() {
    const [, currentHash] = window.location.hash.split('#');
    return currentHash;
}
export function getDefaultLocation(locationAccess: boolean, locations: WeatherLocation[]) {
    const currentHash = getCurrentHash();
    let defaultLocation = currentHash;
    if (locationAccess) {
        defaultLocation = currentHash ? currentHash : CURRENT_LOCATION_KEY;
    } else {
        defaultLocation = locations[0].woeid.toString();
    }
    return defaultLocation;
}


export function isLocationMissing(locations: WeatherLocation[], currentHash: string) {

    return (currentHash != undefined &&
        currentHash != CURRENT_LOCATION_KEY &&
        !locations.filter(({ woeid }) => currentHash === `${woeid}`).length
    )
}