import '@testing-library/jest-dom/extend-expect';

const mockGeolocation = {
    getCurrentPosition: jest.fn(),
    watchPosition: jest.fn(),
    clearWatch: jest.fn()
};

(global as any).navigator.geolocation = mockGeolocation;