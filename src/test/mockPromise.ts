
import { act } from "react-dom/test-utils";

const createMockCallback = (callback: Function) => (...args: any[]) => {
  let result;

  if (!callback) {
    return;
  }

  act(() => {
    result = callback(...args);
  });

  return result;
};

export class MockPromise {
  promise: Promise<any>;
  promiseResolve: (value?: any) => void;
  promiseReject: (value?: any) => void;
  constructor() {
    this.promise = new Promise((resolve, reject) => {
      this.promiseResolve = resolve;
      this.promiseReject = reject;
    });
  }

  resolve(...args: any[]) {
    this.promiseResolve(...args);

    return this;
  }

  reject(...args: Function[]) {
    this.promiseReject(...args);

    return this;
  }

  then(...callbacks: Function[]) {
    const mockCallbacks = callbacks.map(callback =>
      createMockCallback(callback)
    );

    this.promise = this.promise.then(...mockCallbacks);

    return this;
  }

  catch(callback: Function) {
    const mockCallback = createMockCallback(callback);

    this.promise = this.promise.catch(mockCallback);

    return this;
  }

  finally(callback: Function) {
    const mockCallback = createMockCallback(callback);

    this.promise = this.promise.finally(mockCallback);

    return this;
  }
} 