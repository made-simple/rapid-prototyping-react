import { hot } from 'react-hot-loader';
import React from 'react';
import { render } from 'react-dom';
import './style.css';
import App from './components/App';
import { LocationsContextProvider } from './components/LocationsContextProvider';

const HotReload = hot(module)(() => {
  return (
    <LocationsContextProvider>
      <App />
    </LocationsContextProvider>
  );
});
{
  const node = document.getElementById('root');
  if (node) {
    render(<HotReload />, node);
  }
}
