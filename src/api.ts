import dayjs = require("dayjs");
import { isToday } from "./utils";

const WEATHER_API = "https://www.metaweather.com/api"
const CURRENT_LOCATION_KEY = 'current_location';
const LOCATION_KEY = 'location_';
const LOCATIONS_KEY = 'locations';

function get(url: string) {
    return fetch(`https://api.allorigins.win/get?url=${encodeURIComponent(url)}`)
        .then(response => {
            if (response.ok) return response.json()
            throw new Error('Network response was not ok.')
        }).then(response => JSON.parse(response.contents))
}
export function getLocation(coords: Coordinates): Promise<WeatherLocation> {
    return get(`${WEATHER_API}/location/search/?lattlong=${coords.latitude},${coords.longitude}`).then((locations: WeatherLocation[]) => {
        if (!locations.length) {
            return null;
        }
        const [first] = locations;
        setLocationToCache(first);
        return first;
    })
}

export function setLocationToCache(coords: WeatherLocation) {
    localStorage.setItem(CURRENT_LOCATION_KEY, JSON.stringify(coords));
}

export function getLocationFromCache(): WeatherLocation {
    try {
        const weatherLocation: WeatherLocation = JSON.parse(localStorage.getItem(CURRENT_LOCATION_KEY));
        return weatherLocation;

    } catch {
        return null;
    }
    //Check if they are valid


}

export function getLocationsByName(query: string): Promise<WeatherLocation[]> {
    return get(`${WEATHER_API}/location/search/?query=${query}`).then((locations: WeatherLocation[]) => {
        return locations;
    })
}
export function getLocationById(woeid: string): Promise<WeatherLocation> {
    return get(`${WEATHER_API}/location/${woeid}/`).then(res => {
        delete res.consolidated_weather;
        return res;
    });
}

export function getPredictions(woeid: number): Promise<Weather[]> {
    return getPredictionsFromCache(woeid).catch(() => get(`${WEATHER_API}/location/${woeid}/`).then(res => {

        setPredictionsToCache(woeid, res.consolidated_weather);
        return res.consolidated_weather;
    }));
}

export function setPredictionsToCache(woeid: number, predictions: Weather[]) {
    localStorage.setItem(`${LOCATION_KEY}${woeid}`, JSON.stringify(predictions));
}

export function getPredictionsFromCache(woeid: number): Promise<Weather[]> {
    const predictions: Weather[] = JSON.parse(localStorage.getItem(`${LOCATION_KEY}${woeid}`));
    if (Array.isArray(predictions) && predictions.length) {
        const [current] = predictions;
        if (!isToday(dayjs(current.applicable_date))) {
            console.log('invalid prediction');

            return Promise.reject();

        }
        return Promise.resolve(predictions);

    }
    return Promise.reject();


}

export function getLocationsFromStorage() {
    try {
        const locations: WeatherLocation[] = JSON.parse(localStorage.getItem(LOCATIONS_KEY));
        return Array.isArray(locations) ? locations : [];
    } catch {
        return [];
    }
}

export function setLocationsToStorage(locations: WeatherLocation[]) {
    localStorage.setItem(LOCATIONS_KEY, JSON.stringify(locations));
}

