declare module '*.json' {
  const value: any;
  export default value;
}
declare module '*.svg' {
  const content: any;
  export default content;
}

declare module '*.csample' {
  const value: string;
  export default value
}
