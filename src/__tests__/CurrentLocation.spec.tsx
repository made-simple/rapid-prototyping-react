import { render } from '@testing-library/react';
import React from 'react';
import CurrentLocation from '../components/CurrentLocation';

jest.mock('../icons/location.svg', () => ({
  __esModule: true,
  default: () => <div data-testid="icon" />
}));

jest.mock('../components/PredictionLocation', () => ({
  __esModule: true,
  default: () => <div data-testid="location" />
}));
jest.mock('../components/PredictionSummary', () => ({
  __esModule: true,
  default: () => <div data-testid="summary" />
}));
jest.mock('../components/PredictionNextDays', () => ({
  __esModule: true,
  default: () => <div data-testid="nextDays" />
}));

jest.mock('../hooks', () => ({
  useLocation: jest.fn(),
  usePredictions: jest.fn()
}));

import { useLocation, usePredictions } from '../hooks';
const useLocationMock = useLocation as jest.Mock;
const usePredictionsMock = usePredictions as jest.Mock;

const predictions: Weather[] = [
  {
    weather_state_name: 'Showers',
    weather_state_abbr: 's',
    wind_direction_compass: 'NE',
    created: '2019-07-14T09:04:02.332152Z',
    applicable_date: '2019-07-14',
    min_temp: 13.965,
    max_temp: 21.915,
    the_temp: 21.095,
    wind_speed: 4.927574037092712,
    wind_direction: 38.329902118447606,
    air_pressure: 1023.625,
    humidity: 62,
    visibility: 10.088271991569236,
    predictability: 73
  },
  {
    weather_state_name: 'Showers',
    weather_state_abbr: 's',
    wind_direction_compass: 'NE',
    created: '2019-07-14T09:04:02.332152Z',
    applicable_date: '2019-07-15',
    min_temp: 13.965,
    max_temp: 21.915,
    the_temp: 21.095,
    wind_speed: 4.927574037092712,
    wind_direction: 38.329902118447606,
    air_pressure: 1023.625,
    humidity: 62,
    visibility: 10.088271991569236,
    predictability: 73
  }
];
const weatherLocation: WeatherLocation = {
  distance: 0,
  title: 'London',
  location_type: 'City',
  woeid: 123,
  latt_long: ''
};

test('renders a weather view', async () => {
  useLocationMock.mockImplementation(() => [weatherLocation]);
  usePredictionsMock.mockImplementation(() => [predictions]);

  const { queryByTestId } = render(<CurrentLocation />);

  const weatherLocation = queryByTestId('location');
  const summary = queryByTestId('summary');
  const icon = queryByTestId('icon');
  const nextDays = queryByTestId('nextDays');

  expect(weatherLocation).not.toBeNull();
  expect(summary).not.toBeNull();
  expect(icon).not.toBeNull();
  expect(nextDays).not.toBeNull();
});
