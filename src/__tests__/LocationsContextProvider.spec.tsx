import React from 'react';
import { render } from '@testing-library/react';
import { LocationsContext, LocationsContextProvider } from '../components/LocationsContextProvider';
jest.mock('../api', () => ({
  getLocationsFromStorage: jest.fn(),
  setLocationsToStorage: jest.fn()
}));

import { getLocationsFromStorage, setLocationsToStorage } from '../api';
const getLocationsFromStorageMock = getLocationsFromStorage as jest.Mock;
const setLocationsToStorageMock = setLocationsToStorage as jest.Mock;

const weatherLocation: WeatherLocation = {
  distance: 0,
  title: 'London',
  location_type: 'City',
  woeid: 123,
  latt_long: ''
};

test('LocationsContextProvider gets locations from storage', () => {
  getLocationsFromStorageMock.mockImplementation(() => [weatherLocation]);
  setLocationsToStorageMock.mockImplementation(() => null);

  const { getByText } = render(
    <LocationsContextProvider>
      <LocationsContext.Consumer>
        {({ locations }) => <span>{locations[0].woeid}</span>}
      </LocationsContext.Consumer>
    </LocationsContextProvider>
  );

  expect(getLocationsFromStorageMock).toHaveBeenCalled();
  expect(setLocationsToStorageMock).toHaveBeenCalled();

  expect(getByText('123')).not.toBeNull();
});
