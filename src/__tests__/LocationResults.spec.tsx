import { render, fireEvent } from '@testing-library/react';
import React from 'react';
import LocationResults from '../components/LocationResults';

test('render no results', () => {
  const { getByText } = render(<LocationResults locations={[]} onLocationSelected={() => null} />);
  expect(getByText('No results found')).not.toBeNull();
});
test('render results and allow selection', () => {
  const weatherLocation: WeatherLocation = {
    distance: 0,
    title: 'London',
    location_type: 'City',
    woeid: 123,
    latt_long: ''
  };
  const onLocationSelectedMock = jest.fn();
  const { container } = render(
    <LocationResults locations={[weatherLocation]} onLocationSelected={onLocationSelectedMock} />
  );
  const rows = Array.from(container.querySelectorAll('ul li'));
  expect(rows.length).toBe(1);
  expect(rows[0].textContent).toBe('London, City');
  fireEvent.click(rows[0]);
  expect(onLocationSelectedMock).toHaveBeenCalled();
});
