import { render } from '@testing-library/react';
import React from 'react';
import PredictionLocation from '../components/PredictionLocation';

test('shows a location', () => {
  const { getByText } = render(<PredictionLocation name="London" type="City" />);
  expect(getByText('London, City')).toBeInTheDocument();
});
