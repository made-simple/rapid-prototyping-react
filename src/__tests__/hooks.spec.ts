import { renderHook, act } from '@testing-library/react-hooks';
import { getLocation, getLocationFromCache } from '../api';
import { useLocation } from '../hooks';
import { MockPromise } from '../test/utils';

jest.mock('../api', () => ({
    getLocationFromCache: jest.fn(),
    getLocation: jest.fn()
}));

const getLocationFromCacheMock = getLocationFromCache as jest.Mock;
const getLocationMock = getLocation as jest.Mock;
const watchPositionMock = navigator.geolocation.watchPosition as jest.Mock;

afterEach(() => {
    jest.clearAllMocks();
});

test('useLocation should fetch a location from your coordinates', async () => {
    getLocationFromCacheMock.mockImplementation(() => ({}));
    const mockPromiseLocation = new MockPromise();
    getLocationMock.mockImplementation(() => mockPromiseLocation);
    let watchPositionCallback: Function;
    watchPositionMock.mockImplementation(fn => (watchPositionCallback = fn));


    const hook = renderHook(() => useLocation())
    let state = hook.result.current;
    let { weatherLocation } = state;
    expect(weatherLocation).toEqual({});

    expect(watchPositionMock).toHaveBeenCalled();

    act(() => {
        watchPositionCallback({
            coords: {
                accuracy: 150,
                altitude: null,
                altitudeAccuracy: null,
                heading: null,
                latitude: 19.075984,
                longitude: 72.877656,
                speed: null
            },
            timestamp: 1563239868262
        });
    });


    expect(getLocationMock).toHaveBeenCalled();
    const responseLocation = {
        distance: 0,
        title: 'London',
        location_type: 'City',
        woeid: 1234,
        latt_long: ''
    };

    act(() => {
        mockPromiseLocation.resolve(responseLocation);
    });
    await hook.waitForNextUpdate()

    state = hook.result.current;
    weatherLocation = state.weatherLocation;

    expect(weatherLocation).toBe(responseLocation)



})
test('useLocation should return an error if geolocation fails', async () => {
    getLocationFromCacheMock.mockImplementation(() => ({}));
    const mockPromiseLocation = new MockPromise();
    getLocationMock.mockImplementation(() => mockPromiseLocation);
    let watchPositionCallback: Function;
    let watchPositionErrorCallback: Function;
    watchPositionMock.mockImplementation((callback, errorCallback) => {
        watchPositionCallback = callback;
        watchPositionErrorCallback = errorCallback;

    });


    const hook = renderHook(() => useLocation())
    let state = hook.result.current;
    let { error } = state;
    expect(error).toEqual(false);

    expect(watchPositionMock).toHaveBeenCalled();

    act(() => {
        watchPositionErrorCallback({});
    });


    expect(getLocationMock).not.toHaveBeenCalled();

    state = hook.result.current;
    error = state.error;

    expect(error).toBe(true)



})
test('useLocation should return an error if fetch position fails', async () => {
    getLocationFromCacheMock.mockImplementation(() => ({}));
    const mockPromiseLocation = new MockPromise();
    getLocationMock.mockImplementation(() => mockPromiseLocation);
    let watchPositionCallback: Function;
    watchPositionMock.mockImplementation(fn => (watchPositionCallback = fn));


    const hook = renderHook(() => useLocation())
    let state = hook.result.current;
    let { weatherLocation } = state;
    let { error } = state;
    expect(error).toEqual(false);


    expect(weatherLocation).toEqual({});

    expect(watchPositionMock).toHaveBeenCalled();

    act(() => {
        watchPositionCallback({
            coords: {
                accuracy: 150,
                altitude: null,
                altitudeAccuracy: null,
                heading: null,
                latitude: 19.075984,
                longitude: 72.877656,
                speed: null
            },
            timestamp: 1563239868262
        });
    });


    expect(getLocationMock).toHaveBeenCalled();

    act(() => {
        mockPromiseLocation.reject();
    });
    await hook.waitForNextUpdate()

    state = hook.result.current;
    error = state.error;

    expect(error).toBe(true)



})
