import { render } from '@testing-library/react';
import React from 'react';
import GeolocationInstructions from '../components/GeolocationInstructions';

test('shows a geolocation instructions', () => {
  const { container } = render(<GeolocationInstructions />);
  expect(container).not.toBeNull();
});
