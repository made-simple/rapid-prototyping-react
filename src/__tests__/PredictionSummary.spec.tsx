import { render } from '@testing-library/react';
import React from 'react';
import PredictionSummary from '../components/PredictionSummary';

test('shows the state and temperture of the weather', () => {
  const prediction = {
    weather_state_name: 'Showers',
    weather_state_abbr: 's',
    wind_direction_compass: 'NE',
    created: '2019-07-14T09:04:02.332152Z',
    applicable_date: '2019-07-14',
    min_temp: 13.965,
    max_temp: 21.915,
    the_temp: 21.095,
    wind_speed: 4.927574037092712,
    wind_direction: 38.329902118447606,
    air_pressure: 1023.625,
    humidity: 62,
    visibility: 10.088271991569236,
    predictability: 73
  };
  const { getByText, getByTestId } = render(<PredictionSummary prediction={prediction} />);
  expect(getByText('Showers')).toBeInTheDocument();
  const icon = getByTestId('icon') as HTMLImageElement;
  expect(icon.src).toBe('https://www.metaweather.com/static/img/weather/s.svg');
  expect(getByText('21°')).toBeInTheDocument();
});

test('when a null prediction is passed renders placeholder', () => {
  const { getByTestId, container } = render(<PredictionSummary prediction={null} />);
  const icon = getByTestId('icon') as HTMLImageElement;
  expect(icon.tagName).toBe('DIV');
  const [first, second] = Array.from(container.querySelectorAll('span'));
  expect(first.textContent).toBe('--');
  expect(second.textContent).toBe('--');
});
