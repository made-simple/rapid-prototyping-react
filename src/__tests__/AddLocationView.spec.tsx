import { fireEvent, render, waitForElementToBeRemoved } from '@testing-library/react';
import React from 'react';
import { act } from 'react-dom/test-utils';
import { MemoryRouter } from 'react-router';
import { getLocationsByName } from '../api';
import { LocationsContext } from '../components/LocationsContextProvider';
import AddLocationView from '../components/views/AddLocationView';
import { MockPromise } from '../test/utils';

const weatherLocation: WeatherLocation = {
  distance: 0,
  title: 'London',
  location_type: 'City',
  woeid: 123,
  latt_long: ''
};
jest.mock('../api', () => ({
  getLocationsByName: jest.fn()
}));
jest.mock('../components/LocationResults', () => ({
  __esModule: true,
  default: ({
    onLocationSelected
  }: {
    onLocationSelected: (weatherLocation: WeatherLocation) => void;
  }) => <div data-testid="results" onClick={() => onLocationSelected(weatherLocation)} />
}));
jest.mock('../icons/search.svg', () => ({
  __esModule: true,
  default: () => <div data-testid="icon" />
}));

const getLocationsByNameMock = getLocationsByName as jest.Mock;

test('allow search locations and select one', async () => {
  const setLocationsMock = jest.fn();
  const mockPromiselocations = new MockPromise();
  getLocationsByNameMock.mockImplementation(() => mockPromiselocations);
  const { queryByTestId } = render(
    <LocationsContext.Provider value={{ locations: [], setLocations: setLocationsMock }}>
      <MemoryRouter>
        <AddLocationView />
      </MemoryRouter>
    </LocationsContext.Provider>
  );
  jest.useFakeTimers();

  const searchInput = queryByTestId('search');
  expect(queryByTestId('loading')).toBeNull();

  fireEvent.change(searchInput, { target: { value: 'New York' } });

  expect(queryByTestId('loading').textContent).toBe('Validating city...');
  jest.runAllTimers();
  expect(getLocationsByNameMock).toHaveBeenCalled();

  act(() => {
    mockPromiselocations.resolve([{}]);
  });
  jest.useRealTimers();

  await waitForElementToBeRemoved(() => queryByTestId('loading'));
  const results = queryByTestId('results');
  expect(results).not.toBeNull();

  expect(setLocationsMock).not.toHaveBeenCalled();
  fireEvent.click(results);
  expect(setLocationsMock).toHaveBeenCalled();
});
