import { render } from '@testing-library/react';
import React from 'react';
import PredictionNextDays from '../components/PredictionNextDays';
const predictions = [
  {
    weather_state_name: 'Showers',
    weather_state_abbr: 's',
    wind_direction_compass: 'NE',
    created: '2019-07-14T09:04:02.332152Z',
    applicable_date: '2019-07-14',
    min_temp: 13.965,
    max_temp: 21.915,
    the_temp: 21.095,
    wind_speed: 4.927574037092712,
    wind_direction: 38.329902118447606,
    air_pressure: 1023.625,
    humidity: 62,
    visibility: 10.088271991569236,
    predictability: 73
  },
  {
    weather_state_name: 'Showers',
    weather_state_abbr: 's',
    wind_direction_compass: 'NE',
    created: '2019-07-14T09:04:02.332152Z',
    applicable_date: '2019-07-15',
    min_temp: 13.965,
    max_temp: 21.915,
    the_temp: 21.095,
    wind_speed: 4.927574037092712,
    wind_direction: 38.329902118447606,
    air_pressure: 1023.625,
    humidity: 62,
    visibility: 10.088271991569236,
    predictability: 73
  }
];
test('shows the next days weather', () => {
  const { getAllByTestId } = render(<PredictionNextDays predictions={predictions} />);
  const rows = getAllByTestId('weather-row');
  expect(rows.length).toBe(2);
  const [first] = rows;
  const image = first.querySelector('img');
  expect(image.src).toBe('https://www.metaweather.com/static/img/weather/s.svg');
  const [day, min, max] = Array.from(first.querySelectorAll('span'));
  expect(day.textContent).toBe('Sunday');
  expect(min.textContent).toBe('14');
  expect(max.textContent).toBe('22');
});

test('when a list of null predictions is passed renders placeholder', () => {
  const { getAllByTestId } = render(<PredictionNextDays predictions={[null, null]} />);
  const rows = getAllByTestId('weather-row');
  expect(rows.length).toBe(2);
  const [first] = rows;
  const image = first.querySelector('div');
  expect(image).not.toBeNull();
  const [day, min, max] = Array.from(first.querySelectorAll('span'));
  expect(day.textContent).toBe('Someday');
  expect(min.textContent).toBe('--');
  expect(max.textContent).toBe('--');
});
