import { render, waitForElement } from '@testing-library/react';
import React from 'react';
import App from '../components/App';
import { MockPromise } from '../test/utils';
jest.mock('../api', () => ({
  getLocation: jest.fn(),
  getPredictions: jest.fn()
}));
jest.mock('../utils', () => ({
  isLocationMissing: jest.fn(),
  getCurrentHash: () => ''
}));
jest.mock('../components/LocationView', () => ({
  __esModule: true,
  default: () => <div data-testid="location" />
}));

import { getLocation, getPredictions } from '../api';
import { isLocationMissing } from '../utils';
const getLocationMock = getLocation as jest.Mock;
const getPredictionsMock = getPredictions as jest.Mock;
const isLocationMissingMock = isLocationMissing as jest.Mock;
const watchPositionMock = navigator.geolocation.watchPosition as jest.Mock;

test('renders the app', async () => {
  const mockPromiseLocation = new MockPromise();
  getLocationMock.mockImplementation(() => mockPromiseLocation);
  const mockPromiseWeather = new MockPromise();
  getPredictionsMock.mockImplementation(() => mockPromiseWeather);
  let watchPositionCallback: Function;
  watchPositionMock.mockImplementation(fn => (watchPositionCallback = fn));

  isLocationMissingMock.mockImplementation(() => false);

  const { queryByTestId, getAllByText } = render(<App />);

  // expect(getAllByText('Loading')).not.toBeNull();
  // expect(getLocationMock).not.toHaveBeenCalled();
  // expect(getPredictionsMock).not.toHaveBeenCalled();
  // expect(watchPositionMock).toHaveBeenCalled();

  // expect(queryByTestId('summary')).toBeNull();
  // expect(queryByTestId('nextDays')).toBeNull();
  // expect(queryByTestId('location')).toBeNull();

  // act(() => {
  //   watchPositionCallback({
  //     coords: {
  //       accuracy: 150,
  //       altitude: null,
  //       altitudeAccuracy: null,
  //       heading: null,
  //       latitude: 19.075984,
  //       longitude: 72.877656,
  //       speed: null
  //     },
  //     timestamp: 1563239868262
  //   });
  // });

  // expect(getLocationMock).toHaveBeenCalled();

  // mockPromiseLocation.resolve({
  //   distance: 0,
  //   title: 'London',
  //   location_type: 'City',
  //   woeid: 1234,
  //   latt_long: ''
  // });

  // mockPromiseWeather.resolve([{}]);
  // expect(getPredictionsMock).toHaveBeenCalled();

  // const location = await waitForElement<HTMLElement>(() => queryByTestId('location'));
  // expect(location).not.toBeNull();
});
