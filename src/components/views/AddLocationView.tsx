import React, { FunctionComponent, useContext, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { getLocationsByName } from '../../api';
import { ActionsMapper, useCustomReducer } from '../../hooks/reducer';
import SearchSVG from '../../icons/search.svg';
import LinkBack from '../LinkBack';
import LocationResults from '../LocationResults';
import { LocationsContext } from '../LocationsContextProvider';

enum Actions {
  Search = 'Search',
  Results = 'Results'
}

type AddLocationAction = Actions.Search | Actions.Results;

interface AddLocationState {
  results: WeatherLocation[];
  loading: boolean;
  query: string;
}

const mapper: ActionsMapper<AddLocationState, AddLocationAction> = {
  [Actions.Search]: (_, action) => {
    return {
      loading: action.payload.length > 2,
      results: null,
      query: action.payload
    };
  },
  [Actions.Results]: (state, action) => {
    return {
      ...state,
      results: action.payload,
      loading: false
    };
  }
};

export const AddLocationView: FunctionComponent<RouteComponentProps> = ({ history }) => {
  const { locations, setLocations } = useContext(LocationsContext);
  const [state, dispatch] = useCustomReducer<AddLocationState, AddLocationAction>(mapper, {
    results: null,
    loading: false,
    query: ''
  });
  const { query, results, loading } = state;

  useEffect(() => {
    let timeout: any;
    if (loading) {
      timeout = setTimeout(() => {
        getLocationsByName(query).then(results => {
          dispatch({ type: Actions.Results, payload: results });
        });
      }, 700);
    }
    return () => {
      clearTimeout(timeout);
    };
  }, [query]);

  return (
    <div className="h-full md:h-32 w-full p-4 md:w-1/3 ">
      <div className="flex items-center shadow rounded border-0 p-3 text-gray-500 ">
        <SearchSVG className=" w-6 mr-2 path-current fill-current" />
        <input
          data-testid="search"
          type="search"
          className="outline-none w-full"
          placeholder="Search by city name..."
          autoFocus={true}
          onChange={e => {
            const q = e.target.value;

            dispatch({ type: Actions.Search, payload: q });
          }}
        />
        <LinkBack className="ml-2">Cancel</LinkBack>
      </div>

      {loading && (
        <span data-testid="loading" className="block p-2">
          Validating city...
        </span>
      )}
      {results && (
        <LocationResults
          locations={results}
          onLocationSelected={weatherLocation => {
            setLocations([weatherLocation, ...locations]);
            history.replace(`/#${weatherLocation.woeid}`);
          }}
        />
      )}
    </div>
  );
};
export default withRouter(AddLocationView);
