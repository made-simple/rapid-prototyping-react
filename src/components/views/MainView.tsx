import React, { FunctionComponent, useContext, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import { fromEvent } from 'rxjs';
import { debounceTime, throttleTime } from 'rxjs/operators';
import PlusSVG from '../../icons/plus.svg';
import { LocationsContext } from '../LocationsContextProvider';
import { scrollTo, getCurrentHash } from '../../utils';
import CurrentLocation from '../CurrentLocation';
import LocationView from '../LocationView';
import NavigationBullets, { NavigationBulletsHandles } from '../NavigationBullets';
const MainView: FunctionComponent<{ locations: WeatherLocation[]; locationAccess: boolean }> = ({
  locations,
  locationAccess
}) => {
  const { setLocations } = useContext(LocationsContext);
  const wrapperRef = useRef<HTMLDivElement>(null);
  const bulletsRef = useRef<NavigationBulletsHandles>(null);
  console.log('MainView');
  useEffect(() => {
    const onScroll = fromEvent(wrapperRef.current, 'scroll')
      .pipe(
        throttleTime(200),
        debounceTime(100)
      )
      .subscribe(() => {
        const scrollLeft = wrapperRef.current.scrollLeft;
        const threshold = 30;
        const min = scrollLeft - threshold;
        const max = scrollLeft + threshold;
        Array.from(wrapperRef.current.children).forEach(el => {
          //No reliable results
          //const left = el.getBoundingClientRect().left;
          const left = (el as any).offsetLeft;
          if (left > min && left < max) {
            //TODO get location and change the title
            if (getCurrentHash() != el.id) {
              bulletsRef.current.moveTo(el.id);
            }
          }
        });
      });

    return () => {
      onScroll.unsubscribe();
    };
  }, [wrapperRef, bulletsRef]);

  return (
    <div className=" flex flex-col h-full w-full  md:h-auto md:w-1/3 relative">
      <div ref={wrapperRef} className="location-wrapper flex  ">
        {locationAccess && (
          <div id="current" className="w-full flex flex-col items-center flex-shrink-0 py-4">
            <CurrentLocation />
          </div>
        )}
        {locations.map(weatherLocation => (
          <div
            key={weatherLocation.woeid}
            id={`${weatherLocation.woeid}`}
            className="w-full flex flex-col items-center flex-shrink-0 py-4 relative">
            <LocationView
              weatherLocation={weatherLocation}
              onLocationDeleted={weatherLocation => {
                setLocations(locations.filter(({ woeid }) => weatherLocation.woeid !== woeid));
              }}
            />
          </div>
        ))}
      </div>
      <NavigationBullets
        ref={bulletsRef}
        locationAccess={locationAccess}
        locations={locations}
        onBulletClicked={id => scrollTo(wrapperRef.current, id)}
      />
      <div className="flex  h-16 flex-shrink-0 justify-between items-center px-4 border-t border-gray-400">
        <div className="flex">
          <span className="text-sm">MetaWeather</span>
          <span className="text-red-500 text-xs ml-1">beta</span>
        </div>
        <Link to="/add">
          <PlusSVG className="w-8 h-8" />
        </Link>
      </div>
    </div>
  );
};
export default React.memo(MainView);
