import React, { FunctionComponent, useCallback } from 'react';
import ErrorSVG from '../../icons/error.svg';

const ErrorView: FunctionComponent<{}> = () => {
  const goToRoot = useCallback(() => {
    location.reload();
  }, []);
  return (
    <div className=" text-center flex flex-col justify-center items-center h-full p-8  text-xl md:h-auto md:w-1/3 ">
      <ErrorSVG className="w-1/2" />

      <div className="py-8">Looks like something went wrong, try reloading</div>
      <button onClick={goToRoot}>Reload</button>
    </div>
  );
};
export default ErrorView;
