import React, {
  forwardRef,
  RefForwardingComponent,
  useEffect,
  useImperativeHandle,
  useState
} from 'react';
import LocationSVG from '../icons/location.svg';
import { getDefaultLocation } from '../utils';
const CURRENT_LOCATION_KEY = 'current';

export interface NavigationBulletsHandles {
  moveTo(id: string): void;
}
interface NavigationProps {
  locations: WeatherLocation[];
  locationAccess: boolean;
  onBulletClicked: (woid: string) => void;
}

const NavigationBullets: RefForwardingComponent<NavigationBulletsHandles, NavigationProps> = (
  { locations, onBulletClicked, locationAccess },
  ref
) => {
  const [selectedLocation, setSelectedLocation] = useState(
    getDefaultLocation(locationAccess, locations)
  );
  function goTo(id: string, fire: boolean = true) {
    if (fire) {
      onBulletClicked(id);
    } else {
      history.pushState({}, '', `#${id}`);
      setSelectedLocation(id);
    }
  }
  useEffect(() => {
    onBulletClicked(selectedLocation);
  }, []);

  useImperativeHandle(ref, () => {
    return {
      moveTo: id => {
        goTo(id, false);
      }
    };
  });

  return (
    <ul className="location-navigation flex items-center w-full   h-12 justify-center cursor-pointer mt-auto">
      {locationAccess && (
        <li className="" onClick={() => goTo(CURRENT_LOCATION_KEY)}>
          <LocationSVG
            className={`w-3 h-3 block  mr-2 fill-current  ${
              CURRENT_LOCATION_KEY == selectedLocation ? ' text-black' : ' text-gray-400'
            }`}
          />
        </li>
      )}
      {locations.map(weatherLocation => (
        <li
          key={weatherLocation.woeid}
          className=""
          onClick={() => goTo(`${weatherLocation.woeid}`)}>
          <span
            className={`rounded-full  bg-gray-400 w-3 h-3 block mx-2  ${
              weatherLocation.woeid.toString() == selectedLocation ? 'selected' : ''
            }`}
          />
        </li>
      ))}
    </ul>
  );
};

export default React.memo(forwardRef(NavigationBullets));
