import React, { FunctionComponent } from 'react';

export const LocationResults: FunctionComponent<{
  locations: WeatherLocation[];
  onLocationSelected: (weatherLocation: WeatherLocation) => void;
}> = ({ locations, onLocationSelected }) => {
  if (!locations.length) {
    return <span className="block p-2">No results found</span>;
  }
  return (
    <ul className="flex flex-col shadow rounded border-0 mt-1">
      {locations.map(weatherLocation => (
        <li
          key={weatherLocation.woeid}
          className="p-3 p hover:bg-gray-200 cursor-pointer"
          onClick={() => onLocationSelected(weatherLocation)}>{`${weatherLocation.title}, ${
          weatherLocation.location_type
        }`}</li>
      ))}
    </ul>
  );
};
export default LocationResults;
