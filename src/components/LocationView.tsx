import React, { FunctionComponent } from 'react';
import { usePredictions } from '../hooks';
import DeleteSVG from '../icons/delete.svg';
import PredictionLocation from './PredictionLocation';
import PredictionNextDays from './PredictionNextDays';
import PredictionSummary from './PredictionSummary';

export const LocationView: FunctionComponent<{
  weatherLocation: WeatherLocation;
  predictions: Weather[];
  onLocationDeleted: (weatherLocation: WeatherLocation) => void;
}> = ({ weatherLocation, predictions, onLocationDeleted }) => {
  let currentWeather: Weather,
    nexts: Weather[] = [null, null, null, null, null];
  if (predictions) {
    [currentWeather, ...nexts] = predictions;
  }

  return (
    <>
      <div
        data-testid="delete"
        className="absolute top-0 right-0 p-4 cursor-pointer"
        onClick={() => onLocationDeleted(weatherLocation)}>
        <DeleteSVG className=" w-8  stroke-current" />
      </div>
      <PredictionSummary prediction={currentWeather} />
      <PredictionLocation name={weatherLocation.title} type={weatherLocation.location_type} />
      <PredictionNextDays predictions={nexts} />
    </>
  );
};

export const LocationViewWithPredictions: FunctionComponent<{
  weatherLocation: WeatherLocation;
  onLocationDeleted: (weatherLocation: WeatherLocation) => void;
}> = ({ weatherLocation, onLocationDeleted }) => {
  const [predictions] = usePredictions(weatherLocation);
  return (
    <LocationView
      weatherLocation={weatherLocation}
      predictions={predictions}
      onLocationDeleted={onLocationDeleted}
    />
  );
};

export default React.memo(LocationViewWithPredictions);
