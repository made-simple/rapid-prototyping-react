import React, { FunctionComponent } from 'react';
import { MemoryRouter as Router, Redirect, Route } from 'react-router-dom';
import { useGeolocation, withMissingLocation } from '../hooks';
import { PermissionResult } from '../model';
import GeolocationIndicator from './GeolocationInstructions';
import AddLocationView from './views/AddLocationView';
import MainView from './views/MainView';
import ErrorView from './views/ErrorView';

export const App: FunctionComponent<{ locations: WeatherLocation[] }> = ({ locations }) => {
  const geolocationPermission = useGeolocation();

  if (geolocationPermission === null) {
    return null;
  }

  return (
    <div className="fixed inset-0 flex flex-col items-center justify-center font-thin ">
      {geolocationPermission === PermissionResult.Prompt ? (
        <GeolocationIndicator />
      ) : (
        <Router>
          <Route
            exact
            path="/"
            render={() => {
              if (geolocationPermission === PermissionResult.Denied && !locations.length) {
                return <Redirect to="/add" />;
              }
              return (
                <MainView
                  locations={locations}
                  locationAccess={geolocationPermission === PermissionResult.Granted}
                />
              );
            }}
          />
          <Route exact path="/add/" component={AddLocationView} />
          <Route exact path="/error" component={ErrorView} />
        </Router>
      )}
    </div>
  );
};

const AppWithLocations: FunctionComponent = () => {
  const [locations] = withMissingLocation();
  return locations && <App locations={locations} />;
};

export default AppWithLocations;
