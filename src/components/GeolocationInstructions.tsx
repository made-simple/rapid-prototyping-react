import React, { FunctionComponent } from 'react';

const GeolocationInstructions: FunctionComponent<{}> = ({}) => {
  return (
    <div className=" text-center flex flex-col justify-between h-full p-8  text-xl md:h-auto md:w-1/3 ">
      <div className="py-8 ">Please answer the location access request.</div>
      <div className="py-8">
        If you want go check the weather for your current location you need to allow access.
      </div>
    </div>
  );
};
export default GeolocationInstructions;
