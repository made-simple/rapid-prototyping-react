import React, { FunctionComponent, useEffect, useState } from 'react';
import { getLocationsFromStorage, setLocationsToStorage } from '../api';
export const LocationsContext: React.Context<LocationContext> = React.createContext({
  locations: []
});
const { Provider } = LocationsContext;

export const LocationsContextProvider: FunctionComponent = ({ children }) => {
  const [locations, setLocations] = useState<WeatherLocation[]>(getLocationsFromStorage());
  useEffect(() => {
    setLocationsToStorage(locations);
  }, [locations]);

  return <Provider value={{ locations, setLocations }}>{children}</Provider>;
};
