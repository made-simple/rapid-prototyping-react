import { withRouter, RouteComponentProps } from 'react-router';
import React, { FunctionComponent } from 'react';

const LinkBack: FunctionComponent<RouteComponentProps & { className: string }> = ({
  history,
  children,
  className
}) => {
  return (
    <div className={`cursor-pointer ${className}`} onClick={history.goBack}>
      {children}
    </div>
  );
};
export default withRouter(LinkBack);
