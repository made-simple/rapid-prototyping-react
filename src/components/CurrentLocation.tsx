import React, { FunctionComponent } from 'react';
import { useLocation, usePredictions } from '../hooks';
import LocationSVG from '../icons/location.svg';
import PredictionLocation from './PredictionLocation';
import PredictionNextDays from './PredictionNextDays';
import PredictionSummary from './PredictionSummary';
import { Redirect } from 'react-router';

const CurrentLocation: FunctionComponent<{ weatherLocation: WeatherLocation }> = ({
  weatherLocation
}) => {
  const [predictions] = usePredictions(weatherLocation);
  let currentWeather: Weather,
    locationName = '',
    locationType = '',
    nexts: Weather[] = [null, null, null, null, null];
  if (weatherLocation) {
    locationName = weatherLocation.title;
    locationType = weatherLocation.location_type;
  }
  if (predictions) {
    [currentWeather, ...nexts] = predictions;
  }

  return (
    <>
      <PredictionSummary prediction={currentWeather} />
      <div className={`flex items-center ${!weatherLocation ? 'text-white' : ''}`}>
        <LocationSVG className=" w-4 mr-2 mt-1 fill-current" />
        <PredictionLocation name={locationName} type={locationType} />
      </div>
      <PredictionNextDays predictions={nexts} />
    </>
  );
};

export const CurrentLocationMemo = React.memo(
  CurrentLocation,
  ({ weatherLocation: prevLocation }, { weatherLocation: nextLocation }) => {
    const areEqual =
      prevLocation === nextLocation ||
      (prevLocation && prevLocation.woeid) === (nextLocation && nextLocation.woeid);
    return areEqual;
  }
);

export const CurrentLocationWithCoordinates: FunctionComponent = ({}) => {
  const state = useLocation();
  const { weatherLocation, error } = state;

  return error ? (
    <Redirect to="/error" />
  ) : (
    <CurrentLocationMemo weatherLocation={weatherLocation} />
  );
};

export default CurrentLocationWithCoordinates;
