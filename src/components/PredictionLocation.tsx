import React, { FunctionComponent } from 'react';

const PredictionLocation: FunctionComponent<{ name: string; type: string }> = ({ name, type }) => {
  return <span className="text-3xl">{`${name}, ${type}`}</span>;
};

export default PredictionLocation;
