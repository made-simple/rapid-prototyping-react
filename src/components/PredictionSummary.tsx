import React, { FunctionComponent } from 'react';

const PredictionSummary: FunctionComponent<{ prediction: Weather }> = ({ prediction }) => {
  let temperture = '--',
    status = '--',
    image = <div className="w-8 h-8" data-testid="icon" />;
  if (prediction) {
    temperture = `${prediction.the_temp.toFixed(0)}°`;
    status = prediction.weather_state_name;

    image = (
      <img
        className="w-8 h-8"
        data-testid="icon"
        src={`https://www.metaweather.com/static/img/weather/${prediction.weather_state_abbr}.svg`}
      />
    );
  }

  return (
    <div className="text-center">
      <span className={`flex flex-col items-center ${!prediction ? 'text-white' : ''}`}>
        {image}
        {status}
      </span>
      <span className="text-6xl">{temperture}</span>
    </div>
  );
};

export default React.memo(PredictionSummary);
