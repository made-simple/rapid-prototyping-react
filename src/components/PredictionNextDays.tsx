import React, { FunctionComponent } from 'react';
import dayjs from 'dayjs';

const WeatherRow: FunctionComponent<{ prediction: Weather }> = ({ prediction }) => {
  let minTemperture = '--',
    maxTemperture = '--',
    day = 'Someday',
    image = <div className="w-8 h-8" data-testid="icon" />;
  if (prediction) {
    minTemperture = `${prediction.min_temp.toFixed(0)}`;
    maxTemperture = `${prediction.max_temp.toFixed(0)}`;
    day = dayjs(prediction.applicable_date).format('dddd');
    image = (
      <img
        className="w-8"
        data-testid="icon"
        src={`https://www.metaweather.com/static/img/weather/${prediction.weather_state_abbr}.svg`}
      />
    );
  }
  return (
    <div
      data-testid="weather-row"
      className={`flex justify-center my-2 ${!prediction ? 'text-white' : ''}`}>
      <div className="flex flex-1 justify-start mr-auto">
        <span>{day}</span>
      </div>

      <div className="flex flex-1 justify-center">{image}</div>
      <div className="flex flex-1 justify-end w-12 ml-auto">
        <span>{minTemperture}</span>
        <span className="ml-8">{maxTemperture}</span>
      </div>
    </div>
  );
};
const PredictionNextDays: FunctionComponent<{ predictions: Weather[] }> = ({ predictions }) => {
  return (
    <div className="border-t border-gray-400 mt-8 px-4 w-full">
      {predictions.map((prediction, index) => (
        <WeatherRow key={index} prediction={prediction} />
      ))}
    </div>
  );
};

export default React.memo(PredictionNextDays);
