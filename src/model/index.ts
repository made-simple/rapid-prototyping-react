
export enum PermissionResult {
    Prompt = "prompt",
    Granted = "granted",
    Denied = "denied"

}