import { useReducer } from "react";

export interface GenericAction<A> {
    type: A;
    payload?: any;
}
export interface ActionsMapper<S, A> {
    [key: string]: (state: S, action: GenericAction<A>) => S;
}


export function useCustomReducer<S, A extends string>(mapper: ActionsMapper<S, A>, initialState: S) {
    function reducer(state: S, action: GenericAction<A>): S {
        const handler = mapper[action.type];
        let newState;
        if (handler) {
            newState = handler(state, action);
        } else {
            newState = state;
        }

        return newState;
    }

    return useReducer(reducer, initialState)

};