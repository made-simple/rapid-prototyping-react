import { useContext, useEffect, useState } from "react";
import { getLocationById, getPredictions } from '../api';
import { LocationsContext } from "../components/LocationsContextProvider";
import { PermissionResult } from "../model";
import { getCurrentHash, isLocationMissing } from "../utils";

export function usePredictions(weatherLocation: WeatherLocation) {
    const [predictions, setPredictions] = useState<Weather[]>(null);
    useEffect(() => {
        let isPending = true;
        if (weatherLocation) {
            getPredictions(weatherLocation.woeid).then(res => {
                //TODO change for rxjs or any other way of canceling fetch
                if (isPending) {
                    setPredictions(res);
                }
            });
        }
        return () => {
            isPending = false;
        }
    }, [weatherLocation]);

    return [predictions];

}

export function useGeolocation() {
    const [result, setResult] = useState(null);
    useEffect(
        () => {

            let isPending = true;


            const timeout = setTimeout(() => {
                if (isPending) {
                    setResult(PermissionResult.Prompt)
                }
            }, 1000);

            function saveResults(result: string) {
                clearTimeout(timeout);

                if (isPending) {
                    setResult(result)
                }
            }

            if (navigator.permissions) {
                navigator.permissions.query({ name: 'geolocation' })
                    .then(function (permissionStatus) {

                        saveResults(permissionStatus.state);
                        if (permissionStatus.state === PermissionResult.Prompt) {
                            navigator.geolocation.getCurrentPosition(() => { });

                        }

                        permissionStatus.onchange = function () {
                            saveResults(permissionStatus.state);

                        };

                    });
            } else if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((result) => {

                    saveResults(PermissionResult.Granted)
                }, (err) => {

                    saveResults(PermissionResult.Denied)
                });
            } else {

                saveResults(PermissionResult.Denied)
            }



            return () => {
                isPending = false;

                clearTimeout(timeout);
            }
        }
        , []);
    return result;
}


export function withMissingLocation() {
    const { locations, setLocations } = useContext(LocationsContext);
    const currentHash = getCurrentHash()
    const [missing, setMissing] = useState(isLocationMissing(locations, currentHash));

    useEffect(() => {
        let isPending = true;
        if (missing) {
            getLocationById(currentHash).then(weatherLocation => {
                if (isPending) {
                    setLocations([weatherLocation, ...locations]);
                    setMissing(false)
                }
            });
        }
        return () => {
            isPending = false;
        };
    }, []);

    return [missing ? null : locations];
}

export * from './useLocation';