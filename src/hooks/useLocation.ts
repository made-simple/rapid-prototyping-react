import { useCustomReducer, ActionsMapper } from "./reducer";
import { getLocationFromCache, getLocation } from "../api";
import { calculateDistance } from "../utils/coordinates";
import { useEffect } from "react";
import { statement } from "@babel/template";
enum Actions {
    WeatherLocation = 'WeatherLocation',
    Position = 'Position',
    Error = 'Error',
}

type LocationAction = Actions.WeatherLocation | Actions.Position | Actions.Error;

interface LocationState {
    weatherLocation: WeatherLocation;
    position: Position;
    error: boolean;
}

const mapper: ActionsMapper<LocationState, LocationAction> = {
    [Actions.WeatherLocation]: (state, action) => {
        return {
            ...state,
            weatherLocation: action.payload

        };
    },
    [Actions.Position]: (state, action) => {
        return {
            ...state,
            position: action.payload

        };
    },
    [Actions.Error]: (state, action) => {
        return {
            ...state,
            error: action.payload
        };
    }
};


export function useLocation() {

    const [state, dispatch] = useCustomReducer<LocationState, LocationAction>(mapper, {
        weatherLocation: getLocationFromCache(),
        position: null,
        error: false,
    });
    const { weatherLocation, position, error } = state;
    useEffect(() => {
        const watch = navigator.geolocation.watchPosition(newPosition => {
            if (!position) {
                dispatch({ type: Actions.Position, payload: newPosition });
            } else {
                if (
                    position.timestamp != newPosition.timestamp &&
                    calculateDistance(position.coords, newPosition.coords) * 1000 > 100
                ) {

                    dispatch({ type: Actions.Position, payload: newPosition });
                }
            }
        }, (err) => {
            console.error(err)
            dispatch({ type: Actions.Error, payload: true });

        }, { timeout: 10000, enableHighAccuracy: false });
        return () => {
            navigator.geolocation.clearWatch(watch);
        };
    }, [position]);

    useEffect(() => {
        if (position) {
            getLocation(position.coords).then((res: WeatherLocation) => {
                if (!weatherLocation || (weatherLocation && weatherLocation.woeid != res.woeid)) {
                    dispatch({ type: Actions.WeatherLocation, payload: res });
                }
            }).catch((err) => {
                dispatch({ type: Actions.Error, payload: true });

            });
        }
    }, [position]);


    return { weatherLocation, error };

}